import unittest
import jstat

head = ["H1", "H2"]
line = [float(12.0), float(5.5)]

class JStatTest (unittest.TestCase):

    def test_test (self):
        metric, val =jstat.getValuesToSend(head, line,0);
        self.assertEqual("H1", metric)
        self.assertEqual(float(12.0), val)
        metric, val = jstat.getValuesToSend(head, line, 1);
        self.assertEqual("H2", metric)
        self.assertEqual(float(5.5), val)



if __name__ == '__main__':
    unittest.main()