import time
import graphitesend

graphitesend.init(prefix='jstat', graphite_server='localhost', graphite_port=2003)


#                   "S0C": "current_survivor_space_0_capacity_KB",
#                   "S1C": "current_survivor_space_1_capacity_KB",
#                   "S0U": "survivor_space_0_utilization_KB",
#                   "S1U": "survivor_space_1_utilization_KB",
#                   "EC": "current_eden_space_capacity_KB",
#                   "EU": "eden_space_utilization_KB",
#                   "OC": "current_old_space_capacity_KB",
#                   "OU": "old_space_utilization_KB",
#                   "PC": "current_permanent_space_capacity_KB",
#                   "PU": "permanent_space_utilization_KB",
#                   "YGC": "number_of_young_generation_GC_events",
#                   "YGCT": "young_generation_garbage_collection_time",
#                   "FGC": "number_of_stop_the_world_events",
#                   "FGCT": "full_garbage_collection_time",
#                   "GCT": "total_garbage_collection_time"

def getValuesToSend(head, lineSplited, x):
    return head[x], lineSplited[x]



def sendDataToGraphite(head, lineSplited):
    for x in range(1, len(lineSplited)):
        metric, val = getValuesToSend(head, lineSplited, x)
        graphitesend.send(metric, val)

def jstat():
    head = []
    fileName = "jstat.dat"
    file = open(fileName)
    while 1:
        where = file.tell()
        line = file.readline()
        if not line:
            time.sleep(10)
            file.seek(where)
        else:
            print line  # already has newline
            if len(head) == 0:
                head = line.split()
            else:
                lineSplited = line.split()
                sendDataToGraphite(head, lineSplited)

if __name__ == '__main__':
    jstat()